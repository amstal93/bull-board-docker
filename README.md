# Bull Board in a docker container

## Usage:
`docker run -p 80:80 -e REDIS_HOST=redis.example.com registry.gitlab.com/erwineverts/bull-board-docker:latest`

Docker compose:
```yaml
version: '3'
services:
  bull-board:
    image: registry.gitlab.com/erwineverts/bull-board-docker:latest
    environment:
      REDIS_HOST: redis
    ports:
      - '3008:80'
    networks:
      - bull
  redis:
    image: redis:alpine
    volumes:
      - './data/redis:/data'
    networks:
      - bull

networks:
  bull:
```

### Variables

| Variable | Default |
| ---      |  ------  |
| REDIS_HOST | `localhost` |
| REDIS_PORT | `6379` |
| REDIS_PASSWORD | none |
| REDIS_USE_TLS | `false` |
| BULL_PREFIX | `bull` |
| REFRESH_INTERVAL | `10000` |

## Tags

`latest`, `1` : based on version 1 of bull-board
